package com.stackroute.favouriteservice.exception;

public class FoodItemNotFoundException extends Exception {

	private String message;
	
	public FoodItemNotFoundException(String message) {
		super(message);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
