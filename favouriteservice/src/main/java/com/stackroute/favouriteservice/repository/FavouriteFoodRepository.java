package com.stackroute.favouriteservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.stackroute.favouriteservice.model.FavouriteFood;

@Repository
public interface FavouriteFoodRepository extends JpaRepository<FavouriteFood, Integer> {
	
	public FavouriteFood findByFoodIdAndUserId(long foodId, String userId);

	public List<FavouriteFood> findByUserId(String userId);
	
}