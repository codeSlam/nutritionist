package com.stackroute.favouriteservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import com.stackroute.favouriteservice.filter.JwtFilter;

@SpringBootApplication
public class FavouriteserviceApplication {

	@Bean
	public FilterRegistrationBean<JwtFilter> initJwtFilter() {
		final FilterRegistrationBean<JwtFilter> bean = new FilterRegistrationBean<JwtFilter>();
		bean.setFilter(new JwtFilter());
		bean.addUrlPatterns("/api/*");
		return bean;
	}
	
	public static void main(String[] args) {
		SpringApplication.run(FavouriteserviceApplication.class, args);
	}

}