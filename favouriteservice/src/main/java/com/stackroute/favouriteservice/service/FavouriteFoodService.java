package com.stackroute.favouriteservice.service;

import java.util.List;

import com.stackroute.favouriteservice.exception.FoodItemAlreadyExistsException;
import com.stackroute.favouriteservice.exception.FoodItemNotFoundException;
import com.stackroute.favouriteservice.model.FavouriteFood;

public interface FavouriteFoodService {
	
	public FavouriteFood saveFoodItem(FavouriteFood food) throws FoodItemAlreadyExistsException;
	
	public FavouriteFood updateFoodItem(FavouriteFood food) throws FoodItemNotFoundException;

	public boolean deleteFoodItem(int id) throws FoodItemNotFoundException;
	
	public List<FavouriteFood> getMyFavouriteList(String userId);
}
