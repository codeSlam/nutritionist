package com.stackroute.favouriteservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stackroute.favouriteservice.exception.FoodItemAlreadyExistsException;
import com.stackroute.favouriteservice.exception.FoodItemNotFoundException;
import com.stackroute.favouriteservice.model.FavouriteFood;
import com.stackroute.favouriteservice.repository.FavouriteFoodRepository;

@Service
public class FavouriteFoodServiceImpl implements FavouriteFoodService {

	private FavouriteFoodRepository repository;
	
	@Autowired
	public FavouriteFoodServiceImpl(FavouriteFoodRepository repository) {
		this.repository = repository;
	}
	
	@Override
	public FavouriteFood saveFoodItem(FavouriteFood food) throws FoodItemAlreadyExistsException {
		
		final FavouriteFood checkExistFood = this.repository.findByFoodIdAndUserId(food.getFoodId(),
				food.getUserId());
		
		if(checkExistFood != null) {
			throw new FoodItemAlreadyExistsException("Already in Favourites");
		}

		return this.repository.save(food);
	}

	@Override
	public FavouriteFood updateFoodItem(FavouriteFood food) throws FoodItemNotFoundException {
		
		final FavouriteFood checkExistFood = this.repository.findById(food.getId()).orElse(null);
		
		if(checkExistFood == null) {
			throw new FoodItemNotFoundException("Cannot edit. item not found");
		}

		return this.repository.save(food);
		
	}

	@Override
	public boolean deleteFoodItem(int id) throws FoodItemNotFoundException {
		
		final FavouriteFood checkExistFood = this.repository.findById(id).orElse(null);
		
		if(checkExistFood == null) {
			throw new FoodItemNotFoundException("Cannot delete. item not found");
		}
		
		this.repository.deleteById(id);
		
		return true;
	}

	@Override
	public List<FavouriteFood> getMyFavouriteList(String userId) {
		return this.repository.findByUserId(userId);
	}

}