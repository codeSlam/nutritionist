package com.stackroute.favouriteservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

/**
 * @author 737060
 *
 */
@Entity
@Table(name = "favourite_food")
public class FavouriteFood {
	
	/**
	 * auto generated id
	 */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	/**
	 * variable to store 
	 * food id coming from API
	 */
	@Column(name = "food_id")
	private long foodId;

	/**
	 * variable to store 
	 * name of food
	 */
	@Column(name = "name")
	private String name;
	
	/**
	 * variable to store user
	 * id
	 */
	@Column(name = "user_id")
	private String userId;

	/**
	 * variable to store group
	 *
	 */
	@Column(name = "food_group")
	private String group;
	
	/**
	 * Default constructor
	 */
	public FavouriteFood() {
		//Default Constructor stub
	}

	/**
	 * Param constructor
	 */
	public FavouriteFood(int id, long foodId, String name, String userId, String group) {
		this.id = id;
		this.foodId = foodId;
		this.name = name;
		this.userId = userId;
		this.group = group;
	}

	/**
	 * Getters and setters
	 */

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getFoodId() {
		return foodId;
	}

	public void setFoodId(long foodId) {
		this.foodId = foodId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		FavouriteFood that = (FavouriteFood) o;
		return id == that.id &&
				foodId == that.foodId &&
				name.equals(that.name) &&
				userId.equals(that.userId) &&
				group.equals(that.group);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, foodId, name, userId, group);
	}

	@Override
	public String toString() {
		return "FavouriteFood{" +
				"id=" + id +
				", foodId=" + foodId +
				", name='" + name + '\'' +
				", userId='" + userId + '\'' +
				", group='" + group + '\'' +
				'}';
	}
}