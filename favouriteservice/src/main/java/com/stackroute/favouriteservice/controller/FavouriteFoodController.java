package com.stackroute.favouriteservice.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stackroute.favouriteservice.exception.FoodItemAlreadyExistsException;
import com.stackroute.favouriteservice.exception.FoodItemNotFoundException;
import com.stackroute.favouriteservice.model.FavouriteFood;
import com.stackroute.favouriteservice.service.FavouriteFoodService;

import io.jsonwebtoken.Jwts;

@RestController
@CrossOrigin
@RequestMapping(path="/api/v1/favourites")
public class FavouriteFoodController {

	public static final String SECRET_KEY = "pink@@#Floyd()**isTheB33$stB@09nd3V3R&";
	
	@Autowired
	private FavouriteFoodService service;
	
	@PostMapping(path="/food")
	public ResponseEntity<?> saveFavouriteFood(@RequestBody FavouriteFood food,
			HttpServletRequest request) {
		
		String header = request.getHeader("authorization");
		String token = header.substring(7);		
		String userId = Jwts.parser()
                .setSigningKey(SECRET_KEY)
                .parseClaimsJws(token).getBody().getSubject();

		ResponseEntity<?> response;
		
		Map<String, String> map = new HashMap<String, String>();
		
		try {
			food.setUserId(userId);
			final FavouriteFood favFood = this.service.saveFoodItem(food);
			
			if(favFood != null) {
				response = new ResponseEntity<FavouriteFood>(favFood,
						HttpStatus.CREATED);
			} else {
				map.put("message", "Oops! Something went wrong");
				response = new ResponseEntity<Map<String, String>>(map,
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		} catch(FoodItemAlreadyExistsException 	e) {
			map.put("message", e.getMessage());
			response = new ResponseEntity<Map<String, String>>(map,
					HttpStatus.CONFLICT);
		} catch(Exception e) {
			map.put("message", e.getMessage());
			response = new ResponseEntity<Map<String, String>>(map,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return response;
	}

	@PutMapping(path="/food")
	public ResponseEntity<?> updateFavouriteFood(@RequestBody FavouriteFood food,
			HttpServletRequest request) {
		
//		String header = request.getHeader("authorization");
//		String token = header.substring(7);		
//		String userId = Jwts.parser()
//                .setSigningKey(SECRET_KEY)
//                .parseClaimsJws(token).getBody().getSubject();

		ResponseEntity<?> response;
		
		Map<String, String> map = new HashMap<String, String>();
		
		try {
			//food.setUserId(userId);
			final FavouriteFood favFood = this.service.updateFoodItem(food);
			
			if(favFood != null) {
				response = new ResponseEntity<FavouriteFood>(favFood,
						HttpStatus.OK);
			} else {
				map.put("message", "Oops! Something went wrong");
				response = new ResponseEntity<Map<String, String>>(map,
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		} catch(FoodItemNotFoundException e) {
			map.put("message", e.getMessage());
			response = new ResponseEntity<Map<String, String>>(map,
					HttpStatus.CONFLICT);
		} catch(Exception e) {
			map.put("message", e.getMessage());
			response = new ResponseEntity<Map<String, String>>(map,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return response;
	}
	
	@DeleteMapping(path="/food/{id}")
	public ResponseEntity<?> deleteFavouriteFood(@PathVariable int id) {
		
		ResponseEntity<?> response;
		Map<String, String> map = new HashMap<String, String>();
		
		try {
			final boolean flag = this.service.deleteFoodItem(id);
			
			if(flag) {
				map.put("message", "Delete Successful");
				response = new ResponseEntity<Map<String, String>>(map,
						HttpStatus.OK);
			} else {
				map.put("message", "Oops! Something went wrong");
				response = new ResponseEntity<Map<String, String>>(map,
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		} catch(FoodItemNotFoundException e) {
			map.put("message", e.getMessage());
			response = new ResponseEntity<Map<String, String>>(map,
					HttpStatus.CONFLICT);
			
		} catch(Exception e) {
			map.put("message", e.getMessage());
			response = new ResponseEntity<Map<String, String>>(map,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return response;
	}

	@GetMapping(path="/foods")
	public ResponseEntity<?> getMyFavourites(HttpServletRequest request) {
		ResponseEntity<?> response;
		
		String header = request.getHeader("authorization");
		String token = header.substring(7);		
		String userId = Jwts.parser()
                .setSigningKey(SECRET_KEY)
                .parseClaimsJws(token).getBody().getSubject();
		
		response = new ResponseEntity<List<FavouriteFood>>(this.service.getMyFavouriteList(userId), HttpStatus.OK);
		
		return response;
	}
} 