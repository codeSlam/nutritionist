package com.stackroute.favouriteservice.service;

import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.stackroute.favouriteservice.exception.FoodItemAlreadyExistsException;
import com.stackroute.favouriteservice.exception.FoodItemNotFoundException;
import com.stackroute.favouriteservice.model.FavouriteFood;
import com.stackroute.favouriteservice.repository.FavouriteFoodRepository;

public class FavouriteFoodServiceTest {

	@Mock
	private FavouriteFoodRepository repository;
	
	private FavouriteFood food;
	
	private Optional<FavouriteFood> foods;
	
	@InjectMocks
	private FavouriteFoodServiceImpl service;

	@Before
	public void setUp() {
		
		MockitoAnnotations.initMocks(this);
		
		this.food = new FavouriteFood(1,
				123,
				"cheese",
				"john",
				"comapany");
		
		this.foods = Optional.of(this.food);
	}
	
	@Test
	public void foodItemSaveSuccess() throws FoodItemAlreadyExistsException {
		Mockito.when(this.repository.save(this.food)).thenReturn(this.food);
		
		final FavouriteFood food = this.service.saveFoodItem(this.food);
		
		assertThat(food).isEqualTo(this.food);
		Mockito.verify(this.repository, Mockito.times(1)).save(this.food);
	}
	
	@Test(expected = FoodItemAlreadyExistsException.class)
	public void foodItemSavefailure() throws FoodItemAlreadyExistsException {
		Mockito.when(this.repository.findByFoodIdAndUserId(food.getFoodId(), food.getUserId()))
		.thenReturn(this.food);
		
		final FavouriteFood food = this.service.saveFoodItem(this.food);
		
		Mockito.verify(this.repository, Mockito.times(0)).save(this.food);
	}
	
	@Test
	public void foodItemUpdateSuccess() throws FoodItemNotFoundException {
		Mockito.when(this.repository.findById(this.food.getId())).thenReturn(foods);
		
		Mockito.when(this.repository.save(this.food)).thenReturn(this.food);
		
		this.food.setName("new name");
		
		final FavouriteFood food = this.service.updateFoodItem(this.food);
		
		assertThat(food).isEqualTo(this.food);
		
		Mockito.verify(this.repository, Mockito.times(1)).save(this.food);
	}
	
	@Test(expected = FoodItemNotFoundException.class)
	public void foodItemUpdateFailure() throws FoodItemNotFoundException {
		Mockito.when(this.repository.findById(this.food.getId())).thenReturn(Optional.empty());
		
		Mockito.when(this.repository.save(this.food)).thenReturn(this.food);
		
		this.food.setName("new name");
		
		final FavouriteFood food = this.service.updateFoodItem(this.food);
		
		Mockito.verify(this.repository, Mockito.times(0)).save(this.food);
	}
	
	@Test
	public void foodItemDeleteSuccess() throws FoodItemNotFoundException {
		Mockito.when(this.repository.findById(food.getId())).thenReturn(this.foods);
		
		final boolean flag = this.service.deleteFoodItem(this.food.getId());
		
		assertThat(flag).isEqualTo(true);
		
		Mockito.verify(this.repository, Mockito.times(1)).deleteById(food.getId());;
	}
	
	@Test(expected = FoodItemNotFoundException.class)
	public void foodItemDeleteFailure() throws FoodItemNotFoundException {
		Mockito.when(this.repository.findById(food.getId())).thenReturn(Optional.empty());
		
		final boolean flag = this.service.deleteFoodItem(this.food.getId());
		
		Mockito.verify(this.repository, Mockito.times(0)).deleteById(food.getId());;
	}
	
}