package com.stackroute.favouriteservice.repository;

import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.stackroute.favouriteservice.model.FavouriteFood;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class FavouriteRepositoryTest {

	private FavouriteFood food;
	
	@Autowired
	private FavouriteFoodRepository repository;
	
	@Before()
	public void setUp() {
		this.food = new FavouriteFood(1,
				123,
				"cheese",
				"john",
				"Company");
	}
	
	@Test
	public void saveFood() {
		final FavouriteFood food = this.repository.save(this.food);
		assertThat(food).isEqualTo(food);
	}
	
	@Test
	public void updateFood() {
		FavouriteFood food = this.repository.save(this.food);
		assertThat(food).isEqualTo(food);
		food.setName("new name");
		food = this.repository.save(food);
		assertThat(food).isNotEqualTo(this.food);
		assertThat(food.getName()).isEqualTo("new name");
	}
	
	@Test
	public void deleteFood() {
		FavouriteFood food = this.repository.save(this.food);
		assertThat(food).isEqualTo(food);
		this.repository.deleteById(food.getId());
		assertThat(this.repository.findById(food.getId())).isEqualTo(Optional.empty());
	}
	
	@Test
	public void findMyMovies() {
		
		this.repository.save(new FavouriteFood(2,
				1234,
				"butter",
				"tanny",
				"Company"));
		
		this.repository.save(new FavouriteFood(1,
				123,
				"cheese",
				"john",
				"Company"));
		
		List<FavouriteFood> foods = this.repository.findByUserId("john");
		assertThat(foods.get(0).getName()).isEqualTo("cheese");
		
	}	
}