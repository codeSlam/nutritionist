package com.stackroute.favouriteservice.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stackroute.favouriteservice.exception.FoodItemAlreadyExistsException;
import com.stackroute.favouriteservice.exception.FoodItemNotFoundException;
import com.stackroute.favouriteservice.model.FavouriteFood;
import com.stackroute.favouriteservice.service.FavouriteFoodService;

@RunWith(SpringRunner.class)
@WebMvcTest(FavouriteFoodController.class)
public class FavouriteFoodControllerTest {

	private static final String TEST_JWT_TOKEN = 
			"Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0YW5ueSIsImlhdCI6MTU1MzY4NDY0NX0.mQGcfZZnfPb6p7MVUvPTtAw5GueD-YXmKJ_ajT7KB98";
	
	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private FavouriteFoodService service;
	
	@InjectMocks
	private FavouriteFoodController controller;
	
	private FavouriteFood food;
	
	private List<FavouriteFood> foods;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		
		this.foods = new ArrayList<FavouriteFood>();
		
		this.food = new FavouriteFood(1,
				123,
				"cheese",
				"tanny",
				"company");
		
		this.foods.add(food);
		
		this.food = new FavouriteFood(2,
				123,
				"butter",
				"tanny",
				"company");
		
		this.foods.add(food);
		
	}
	
	@Test
	public void testSaveMovieSuccess() throws FoodItemAlreadyExistsException, Exception {
		
		Mockito.when(this.service.saveFoodItem(this.food)).thenReturn(this.food);
		
		this.mvc.perform(MockMvcRequestBuilders.post("/api/v1/favourites/food")
				.header("authorization", TEST_JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonToString(this.food)))
		.andExpect(MockMvcResultMatchers.status().isCreated())
				.andDo(MockMvcResultHandlers.print());
		
		Mockito.verify(this.service, Mockito.times(1)).saveFoodItem(this.food);
		Mockito.verifyNoMoreInteractions(this.service);	
	}
	
	@Test
	public void testSaveMovieFailure() throws FoodItemAlreadyExistsException, Exception {
		
		Mockito.when(this.service.saveFoodItem(this.food))
		.thenThrow(FoodItemAlreadyExistsException.class);
		
		this.mvc.perform(MockMvcRequestBuilders.post("/api/v1/favourites/food")
				.header("authorization", TEST_JWT_TOKEN)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonToString(this.food)))
		.andExpect(MockMvcResultMatchers.status().isConflict())
				.andDo(MockMvcResultHandlers.print());
		
		Mockito.verify(this.service, Mockito.times(1)).saveFoodItem(this.food);
		Mockito.verifyNoMoreInteractions(this.service);	
	}
	
	@Test
	public void testUpdateMovieSuccess() throws FoodItemNotFoundException, Exception {
		
		Mockito.when(this.service.updateFoodItem(this.food)).thenReturn(this.food);
		
		this.mvc.perform(MockMvcRequestBuilders.put("/api/v1/favourites/food")
				.header("authorization", TEST_JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonToString(this.food)))
		.andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print());
		
		Mockito.verify(this.service, Mockito.times(1)).updateFoodItem(this.food);
		Mockito.verifyNoMoreInteractions(this.service);	
	}
	
	@Test
	public void testUpdateMovieFailure() throws FoodItemNotFoundException, Exception {
		
		Mockito.when(this.service.updateFoodItem(this.food))
		.thenThrow(FoodItemNotFoundException.class);
		
		this.mvc.perform(MockMvcRequestBuilders.put("/api/v1/favourites/food")
				.header("authorization", TEST_JWT_TOKEN)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonToString(this.food)))
		.andExpect(MockMvcResultMatchers.status().isConflict())
				.andDo(MockMvcResultHandlers.print());
		
		Mockito.verify(this.service, Mockito.times(1)).updateFoodItem(this.food);
		Mockito.verifyNoMoreInteractions(this.service);	
	}
	
	@Test
	public void testDeleteMovieSuccess() throws FoodItemNotFoundException, Exception {
		
		Mockito.when(this.service.deleteFoodItem(2)).thenReturn(true);
		
		this.mvc.perform(MockMvcRequestBuilders.delete("/api/v1/favourites/food/{id}", 2)
				.header("authorization", TEST_JWT_TOKEN))
		.andExpect(MockMvcResultMatchers.status().isOk());
		
		Mockito.verify(this.service, Mockito.times(1)).deleteFoodItem(2);
		Mockito.verifyNoMoreInteractions(this.service);	
	}
	
	@Test
	public void testDeleteMovieFailure() throws FoodItemNotFoundException, Exception {
		
		Mockito.when(this.service.deleteFoodItem(2))
		.thenThrow(FoodItemNotFoundException.class);
		
		this.mvc.perform(MockMvcRequestBuilders.delete("/api/v1/favourites/food/{id}", 2)
				.header("authorization", TEST_JWT_TOKEN))
		.andExpect(MockMvcResultMatchers.status().isConflict());
		
		Mockito.verify(this.service, Mockito.times(1)).deleteFoodItem(2);
		Mockito.verifyNoMoreInteractions(this.service);	
	}
	
	@Test
	public void testGetMyFavourites() throws Exception {
		
		Mockito.when(this.service.getMyFavouriteList(this.food.getUserId()))
		.thenReturn(foods);
		
		this.mvc.perform(MockMvcRequestBuilders.get("/api/v1/favourites/foods")
				.header("authorization", TEST_JWT_TOKEN))
		.andExpect(MockMvcResultMatchers.status().isOk());
		
		Mockito.verify(this.service, Mockito.times(1)).getMyFavouriteList(this.food.getUserId());
		Mockito.verifyNoMoreInteractions(this.service);	
	}
	
	//method to map class objects to JSON response
	private static String jsonToString(final Object object) throws JsonProcessingException {
		String result;
		try {
			final ObjectMapper mapper = new ObjectMapper();
			final String jsonContent = mapper.writeValueAsString(object);
			result = jsonContent;
		} catch(JsonProcessingException e) {
			result = "Json processing error";
		}
		return result;
	}
}