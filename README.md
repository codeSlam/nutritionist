# Steps to run
1) First clone the repository into your system.
2) Open terminal and cd to the git repository.
3) Run env-variable file using the following command
    `source env-variable.sh`
4) Run build.sh file using the following command
    `source build.sh`
5) Stop mySql service by using `sudo service mysql stop`   
6) Run `sudo docker-compose up --build` to run the project
7) Naviate to http://localhost:4200
8) To stop the project simply press ctr+c inside terminal
9) To remove containers from docker run `sudo docker-compose down`