package com.stackroute.userservice.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stackroute.userservice.exception.UserAlreadyExistsException;
import com.stackroute.userservice.exception.UserNotFoundException;
import com.stackroute.userservice.model.User;
import com.stackroute.userservice.service.TokenGeneratorService;
import com.stackroute.userservice.service.UserService;

@RestController
@CrossOrigin
@RequestMapping(path="/api/v1/auth")
public class UserController {

	@Autowired
	private UserService service;
	
	@Autowired
	private TokenGeneratorService tokenService;
	
	@PostMapping(path="/register")
	public ResponseEntity<?> registerUser(@RequestBody User user) {
		
		ResponseEntity<?> response;
		Map<String, String> map = new HashMap<String, String>();
		
		try {
			
			final User registeredUser = this.service.registerUser(user);
			if(registeredUser != null) {
				map.put("message", "User Registered Successfully");
				response = new ResponseEntity<Map<String, String>>(map, HttpStatus.CREATED);
			} else {
				map.put("message", "Oops! Something went wrong");
				response = new ResponseEntity<Map<String, String>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		} catch(UserAlreadyExistsException e) {
			map.put("message", e.getMessage());
			response = new ResponseEntity<Map<String, String>>(map, HttpStatus.CONFLICT);
			
		} catch(Exception e) {
			map.put("message", "Oops! Something went wrong");
			response = new ResponseEntity<Map<String, String>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return response;
	}
	
	@PostMapping(path="/login")
	public ResponseEntity<?> loginUser(@RequestBody User user) {
		
		ResponseEntity<?> response;
		Map<String, String> map = new HashMap<String, String>();
		
		try {
			
			final User loggedUser = this.service.loginUser(user);
			
			if(loggedUser != null) {
				map = this.tokenService.generateJwt(loggedUser);
				response = new ResponseEntity<Map<String, String>>(map, HttpStatus.OK);
			} else {
				map.put("message", "Oops! Something went wrong");
				response = new ResponseEntity<Map<String, String>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		} catch(UserNotFoundException e) {
			map.put("message", e.getMessage());
			response = new ResponseEntity<Map<String, String>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
			
		} catch(Exception e) {
			map.put("message", "Oops! Something went wrong");
			response = new ResponseEntity<Map<String, String>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return response;
	}
}