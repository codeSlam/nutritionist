package com.stackroute.userservice.exception;

public class UserAlreadyExistsException extends Exception {
	
	private String message;
	
	public UserAlreadyExistsException(String message) {
		super(message);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
