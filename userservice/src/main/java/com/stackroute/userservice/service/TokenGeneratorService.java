package com.stackroute.userservice.service;

import java.util.Map;

import com.stackroute.userservice.model.User;

public interface TokenGeneratorService {
	public Map<String, String> generateJwt(User user);
}
