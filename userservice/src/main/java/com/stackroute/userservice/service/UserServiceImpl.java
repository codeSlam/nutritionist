package com.stackroute.userservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stackroute.userservice.exception.UserAlreadyExistsException;
import com.stackroute.userservice.exception.UserNotFoundException;
import com.stackroute.userservice.model.User;
import com.stackroute.userservice.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	private UserRepository repository;
	
	@Autowired
	public UserServiceImpl(UserRepository repository) {
		this.repository = repository;
	}
	
	@Override
	public User loginUser(User user) throws UserNotFoundException {
		final User checkUser = this.repository.findByUserId(user.getUserId());
		
		if(checkUser == null) {
			throw new UserNotFoundException("User doesnot exist please register");
		}
		
		else {
			if(checkUser.getPassword().equals(user.getPassword())) {
				return checkUser;
			}
			else {
				throw new UserNotFoundException("Invalid Password");
			}
		}
	}

	@Override
	public User registerUser(User user) throws UserAlreadyExistsException {
		final User checkUser = this.repository.findByUserId(user.getUserId());
		
		if(checkUser != null) {
			throw new UserAlreadyExistsException("User already exists");
		}
		
		else {
			return this.repository.save(user);
		}
	}
}