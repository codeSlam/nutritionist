package com.stackroute.userservice.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.stackroute.userservice.model.User;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class JwtGenerator implements TokenGeneratorService {
	
	private static final String SECRET_KEY = "pink@@#Floyd()**isTheB33$stB@09nd3V3R&";
	
	@Override
	public Map<String, String> generateJwt(User user) {
		
		Map<String, String> map = new HashMap<String, String>();
		
		String jwtToken = "";
		
		jwtToken = Jwts.builder()
				.setSubject(user.getUserId())
				.setIssuedAt(new Date())
				.signWith(SignatureAlgorithm.HS256, SECRET_KEY)
				.compact();
		
		map.put("token", jwtToken);
		map.put("message", "Login Success");
		
		return map;
	}

}