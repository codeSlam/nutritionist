package com.stackroute.userservice.service;

import com.stackroute.userservice.exception.UserAlreadyExistsException;
import com.stackroute.userservice.exception.UserNotFoundException;
import com.stackroute.userservice.model.User;

public interface UserService {

	public User loginUser(User user) throws UserNotFoundException;
	
	public User registerUser(User user) throws UserAlreadyExistsException;
	
}
