package com.stackroute.userservice.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import static org.assertj.core.api.Assertions.assertThat;

import com.stackroute.userservice.exception.UserAlreadyExistsException;
import com.stackroute.userservice.exception.UserNotFoundException;
import com.stackroute.userservice.model.User;
import com.stackroute.userservice.repository.UserRepository;

public class UserServiceTest {

	private User user;
	
	@Mock
	private UserRepository repository;
	
	@InjectMocks
	private UserServiceImpl service;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		
		this.user = new User(1,
				"Johnny",
				"Tans",
				"john",
				"qwerty"
				);
	}
	
	@Test
	public void testRegisterUserSuccess() throws UserAlreadyExistsException {
		Mockito.when(this.repository.save(this.user)).thenReturn(user);
		
		final User user = this.service.registerUser(this.user);
		
		assertThat(user).isEqualTo(this.user);
		
		Mockito.verify(this.repository, Mockito.times(1)).save(this.user);
	}
	
	@Test(expected = UserAlreadyExistsException.class)
	public void testRegisterUserFailure() throws UserAlreadyExistsException {
		Mockito.when(this.repository.findByUserId(this.user.getUserId())).thenReturn(this.user);
		
		final User user = this.service.registerUser(this.user);
		
		Mockito.verify(this.repository, Mockito.times(0)).save(this.user);
	}
	
	@Test
	public void testLoginSuccess() throws UserNotFoundException {
		Mockito.when(this.repository.findByUserId(this.user.getUserId())).thenReturn(this.user);
		
		final User user = this.service.loginUser(this.user);
		
		assertThat(user).isEqualTo(this.user);
		
		Mockito.verify(this.repository, Mockito.times(1)).findByUserId(this.user.getUserId());
	}
	
	@Test(expected = UserNotFoundException.class)
	public void testUserLoginFaliure() throws UserNotFoundException {
		Mockito.when(this.repository.findByUserId(this.user.getUserId())).thenReturn(null);
		
		final User user = this.service.loginUser(this.user);
		
		assertThat(user).isNull();
		
		Mockito.verify(this.repository, Mockito.times(1)).findByUserId(this.user.getUserId());
	}
}