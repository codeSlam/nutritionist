package com.stackroute.userservice.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.assertj.core.api.Assertions.assertThat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.stackroute.userservice.model.User;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class UserRepositoryTest {

	@Autowired
	private UserRepository repository;
	
	private User user;
	
	@Before()
	public void setUp() {
		this.user = new User(1,
				"Johnny",
				"Tans",
				"john",
				"qwerty"
				);
	}
	
	@Test
	public void testRegisterUser() throws Exception {
		final User user = this.repository.save(this.user);
		assertThat(user).isEqualTo(this.user);
	}
}