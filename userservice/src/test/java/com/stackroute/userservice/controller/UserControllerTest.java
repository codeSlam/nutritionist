package com.stackroute.userservice.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.Mockito.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stackroute.userservice.exception.UserAlreadyExistsException;
import com.stackroute.userservice.exception.UserNotFoundException;
import com.stackroute.userservice.model.User;
import com.stackroute.userservice.service.JwtGenerator;
import com.stackroute.userservice.service.UserServiceImpl;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private UserServiceImpl service;
	
	@MockBean
	private JwtGenerator tokenService;
	
	@InjectMocks
	private UserController controller;
	
	private User user;
	
	@Before()
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		
		this.user = new User(1,
				"Johnny",
				"Tans",
				"john",
				"qwerty"
				);
	}
	
	@Test
	public void testRegisterUser() throws UserAlreadyExistsException, Exception {
		Mockito.when(this.service.registerUser(this.user)).thenReturn(this.user);
		
		this.mvc.perform(MockMvcRequestBuilders.post("/api/v1/auth/register")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonToString(this.user)))
		.andExpect(MockMvcResultMatchers.status().isCreated())
		.andDo(MockMvcResultHandlers.print());
		
		
		verify(this.service, times(1)).registerUser(this.user);
		verifyNoMoreInteractions(this.service);
	}
	
	@Test
	public void testLoginUser() throws UserNotFoundException, Exception {
		Mockito.when(this.service.loginUser(this.user)).thenReturn(this.user);
		
		this.mvc.perform(MockMvcRequestBuilders.post("/api/v1/auth/login")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonToString(this.user)))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andDo(MockMvcResultHandlers.print());
		
		verify(this.service, times(1)).loginUser(this.user);
		verifyNoMoreInteractions(this.service);
	}
	
	@Test
	public void testRegisterUserFailure() throws UserAlreadyExistsException, Exception {
		Mockito.when(this.service.registerUser(this.user)).thenReturn(null);
		
		this.mvc.perform(MockMvcRequestBuilders.post("/api/v1/auth/register")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonToString(this.user)))
		.andExpect(MockMvcResultMatchers.status().isInternalServerError())
		.andDo(MockMvcResultHandlers.print());
		
		verify(this.service, times(1)).registerUser(this.user);
		verifyNoMoreInteractions(this.service);
	}
	
	@Test
	public void testLoginUserFailure() throws UserNotFoundException, Exception {
		Mockito.when(this.service.loginUser(this.user)).thenReturn(null);
		
		this.mvc.perform(MockMvcRequestBuilders.post("/api/v1/auth/login")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonToString(this.user)))
		.andExpect(MockMvcResultMatchers.status().isInternalServerError())
		.andDo(MockMvcResultHandlers.print());
		
		verify(this.service, times(1)).loginUser(this.user);
		verifyNoMoreInteractions(this.service);
	}
	
	
	//method to map class objects to JSON response
	private static String jsonToString(final Object object) throws JsonProcessingException {
		String result;
		try {
			final ObjectMapper mapper = new ObjectMapper();
			final String jsonContent = mapper.writeValueAsString(object);
			result = jsonContent;
		} catch(JsonProcessingException e) {
			result = "Json processing error";
		}
		return result;
	}
}