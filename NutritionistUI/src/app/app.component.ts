import { Component } from '@angular/core';
import { AuthenticationService } from './modules/authentication/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Nutritionist';
  isLoggedIn: boolean = false;

  constructor(private _service: AuthenticationService,
    private router: Router) {

  }

  ngOnInit() {
    if(this._service.getToken() != null) {
      this.isLoggedIn = true;
    }
    else {
      this.isLoggedIn = false;
    }
    this._service.loginChangeObservable.subscribe(res => {
      this.isLoggedIn = true;
    });
  }

  logout() {
    this._service.removeToken();
    this.router.navigate(['/login']);
    this.isLoggedIn = false;
  }

}
