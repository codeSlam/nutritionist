import { Injectable } from "@angular/core";

import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from './modules/authentication/auth.service';

@Injectable( {
    providedIn: "root"
})
export class AuthGuardService implements CanActivate {
    
    constructor(private _service: AuthenticationService,
        private router:Router) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if(!this._service.isTokenExpired()) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    }
}