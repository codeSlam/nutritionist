export interface Nutrients {
    name: string;
    unit: string;
    values: string;
    measures: any;
}