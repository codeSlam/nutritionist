import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from '../shared/angular-material/angular-material.module';
import { FavouriteFoodService } from './favourite-food.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './http-interceptor.service';
import { ViewComponent } from './components/view/view.component';
import { SearchComponent } from './components/search/search.component';
import { ContainerComponent } from './components/container/container.component';
import { FoodRoutingModule } from './food-routing.module';
import { FoodApiService } from './food-api.service';
import { NutrientsDialogComponent } from './components/nutrients-dialog/nutrients-dialog.component';
import { FavouritesComponent } from './components/favourites/favourites.component';

@NgModule({
  declarations: [
    ViewComponent,
    SearchComponent, 
    ContainerComponent, 
    NutrientsDialogComponent, 
    FavouritesComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    FoodRoutingModule
  ],
  entryComponents: [
    NutrientsDialogComponent
  ],
  providers: [
    FavouriteFoodService, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
    },
    FoodApiService
  ]
})
export class FoodModule { }