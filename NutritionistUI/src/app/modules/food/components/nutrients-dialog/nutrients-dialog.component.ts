import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'food-nutrients-dialog',
  templateUrl: './nutrients-dialog.component.html',
  styleUrls: ['./nutrients-dialog.component.css']
})
export class NutrientsDialogComponent implements OnInit {

  displayedColumns: string[] = ['nutrient', 'value'];

  constructor(@Inject(MAT_DIALOG_DATA) public nutrients,
  private dialogRef: MatDialogRef<NutrientsDialogComponent>) { }

  ngOnInit() {
  }

  onClose() {
    this.dialogRef.close();
  }

}
