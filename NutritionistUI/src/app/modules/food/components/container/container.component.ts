import { Component, OnInit, Input } from '@angular/core';
import { Food } from '../../food.model';
import { FavouriteFoodService } from '../../favourite-food.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'food-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css']
})
export class ContainerComponent implements OnInit {

  @Input()
  foods: Array<Food>;

  @Input()
  isSearch: boolean;

  constructor(private _service: FavouriteFoodService,
    private _snackBar:MatSnackBar) { }

  ngOnInit() {
  }

  addToFavourites(food) {
    
    this._service.saveFavouriteFood(food)
    .subscribe((response: any) => {

      let matSnackBarRef = this._snackBar.open("Added to Favourites", '',
        {
          duration: 2000
        });

    },
    error => {

      let matSnackBarRef = this._snackBar.open(error.error.message, '',
        {
          duration: 2000
        });

    });
  }

  updateFavourites(food) {
    this._service.updateFavouriteFood(food).subscribe((response: any) => {

      let matSnackBarRef = this._snackBar.open("Update Success!", '',
      {
        duration: 2000
      });
      
    },
    error => {

      let matSnackBarRef = this._snackBar.open(error.error.message, '',
      {
        duration: 2000
      });

    });
  }

  deleteFavourites(food) {
    this._service.deleteFavouriteFood(food.id).subscribe(response => {
      
      const index = this.foods.indexOf(food);

      this.foods.splice(index, 1);

      let matSnackBarRef = this._snackBar.open("Deleted from Favourites", '',
        {
          duration: 2000
        });

    },
    error => {

      let matSnackBarRef = this._snackBar.open(error.error.message, '',
      {
        duration: 2000
      });

    });
  }

}
