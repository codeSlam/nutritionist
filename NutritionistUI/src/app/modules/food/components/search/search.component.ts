import { Component, OnInit } from '@angular/core';
import { FoodApiService } from '../../food-api.service';
import { Food } from '../../food.model';

@Component({
  selector: 'food-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  isSearch = true;
  foods: Array<Food>;

  constructor(private _service: FoodApiService) { }

  ngOnInit() {
  }

  onEnter(searchKey) {
    this._service.searchFood(searchKey)
    .subscribe((response: any) => {
      if(response.list) {
        this.foods = response.list.item;
      }
      else {
        this.foods = [];
      }    
    });
  }

}