import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Food } from '../../food.model';
import { FoodApiService } from '../../food-api.service';
import { Nutrients } from '../../nutrients.model';
import { MatDialog } from '@angular/material/dialog';
import { NutrientsDialogComponent } from '../nutrients-dialog/nutrients-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'food-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  @Input()
  food: Food;

  @Input()
  isSearch: boolean;

  @Output()
  addFood = new EventEmitter();

  @Output()
  updateFood = new EventEmitter();

  @Output()
  deleteFood = new EventEmitter();

  title: string;

  isDoingUpdate: boolean = false;

  newName: string;

  nutrients: Array<Nutrients>;

  content: Array<string>;

  constructor(private _service: FoodApiService, 
    private dialog: MatDialog,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.content = this.food.name.split(",");
    this.title = this.content[0];
    this.content.splice(0, 1);
  }

  getNutritionInfo(id) {
    this._service.getNutrition(id).subscribe((response: any) => {

      if(response.foods[0].food != null) {

        this.nutrients = response.foods[0].food.nutrients;
      
        let dialogRef = this.dialog.open( NutrientsDialogComponent,{
          width:"500px",
          maxWidth: '500px',
          data: this.nutrients
        });

      }
      else {
        this.snackBar.open("No Nutrient Info for the Item", "", 
        {
          duration: 2000
        });
      }
      
    });
  }

  addToFavourites() {
    this.food.foodId = this.food.ndbno;
    this.addFood.emit(this.food);
  }

  updateFavourites() {
    if(this.newName != null && this.newName != ""){
      this.food.name = this.newName;
      this.isDoingUpdate = false;
    }
    this.updateFood.emit(this.food);
  }

  deleteFromFavourites() {
    this.deleteFood.emit(this.food);
  }

}