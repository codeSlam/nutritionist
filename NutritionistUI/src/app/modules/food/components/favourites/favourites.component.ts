import { Component, OnInit } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
import { AuthenticationService } from 'src/app/modules/authentication/auth.service';
import { FavouriteFoodService } from '../../favourite-food.service';
import { Food } from '../../food.model';

@Component({
  selector: 'food-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.css']
})
export class FavouritesComponent implements OnInit {

  user: string;
  foods: Array<Food>;

  constructor(private _authService: AuthenticationService,
    private _favouriteFoodService: FavouriteFoodService) { }

  ngOnInit() {
    this.user = this._authService.getSubject();
    this._favouriteFoodService.getMyFavourites()
    .subscribe((response: any) => {
      this.foods = response;
    });
  }

}
