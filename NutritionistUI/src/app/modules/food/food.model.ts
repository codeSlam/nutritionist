export interface Food {
    id: number;
    foodId: number;
    group: string;
    name: string;
    userId: string;
    ndbno: number;
}