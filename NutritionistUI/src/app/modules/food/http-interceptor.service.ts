import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../authentication/auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor{

    constructor(private _service: AuthenticationService) {}

    intercept(request: HttpRequest<any>, next:HttpHandler): Observable<HttpEvent<any>> {
        if(request.url.startsWith("https://api.nal.usda.gov")) {
            return next.handle(request);
        }
        request = request.clone({
            setHeaders: {
                Authorization: `Bearer ${this._service.getToken()}`
            }
        });
        return next.handle(request);
    }
}