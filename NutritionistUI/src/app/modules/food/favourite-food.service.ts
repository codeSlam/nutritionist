import { Injectable } from "@angular/core";
import { Food } from './food.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: "root"
})
export class FavouriteFoodService {

    springEndPoint: string;

    constructor(private _http: HttpClient) {
        this.springEndPoint = "http://localhost:9090/api/v1/favourites/";
    }

    saveFavouriteFood(item: Food) {
        return this._http.post(`${this.springEndPoint}food`, item);
    }

    updateFavouriteFood(item: Food) {
        return this._http.put(`${this.springEndPoint}food`, item);
    }

    deleteFavouriteFood(id: number) {
        return this._http.delete(`${this.springEndPoint}food/${id}`);
    }

    getMyFavourites() {
        return this._http.get(`${this.springEndPoint}foods`);
    }
}