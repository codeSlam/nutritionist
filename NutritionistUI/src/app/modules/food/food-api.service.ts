import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: "root"
})
export class FoodApiService {

    searchEndPoint: string;
    nutritionEndPoint: string;
    apiKey: string;

    constructor(private _http: HttpClient) {
        this.searchEndPoint = "https://api.nal.usda.gov/ndb/search/?format=json";
        this.nutritionEndPoint = "https://api.nal.usda.gov/ndb/V2/reports?";
        this.apiKey = "&api_key=THVJ0nXhCgZfJyx53rPGHac888uv4WBCidK7Vk3A";
    }

    searchFood(searchKey) {
        const queryParam = `&q=${searchKey}`;
        const searchUrl = `${this.searchEndPoint}${this.apiKey}${queryParam}`;
        return this._http.get(searchUrl);
    }

    getNutrition(foodId) {
        const queryParam = `&ndbno=${foodId}`;
        const searchUrl = `${this.nutritionEndPoint}${this.apiKey}${queryParam}`;
        return this._http.get(searchUrl);
    }
}