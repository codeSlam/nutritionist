import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './components/search/search.component';
import { FavouritesComponent } from './components/favourites/favourites.component';
import { AuthGuardService } from 'src/app/auth-guard.service';

const routes: Routes = [
  {
    path: "food",
    children: [
        {
          path: "",
          redirectTo: "/food/favourites",
          pathMatch: "full",
          canActivate: [AuthGuardService]
        },
        {
            path: "search",
            component: SearchComponent,
            canActivate: [AuthGuardService]
        },
        {
          path: "favourites",
          component: FavouritesComponent,
          canActivate: [AuthGuardService]
        }
    ],
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class FoodRoutingModule { }