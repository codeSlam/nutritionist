import { TestBed, fakeAsync, inject } from '@angular/core/testing';

import { AuthenticationService } from './auth.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

const raw = {
  registerUser: {
      firstName: 'John',
      lastName: 'tanny',
      userId: 'johnnytanny',
      password: 'testpass'
  },

  loginUser: {
      lastName: "",
      firstName: "",
      userId: 'johnnytanny',
      password: 'testpass'
  }
}

describe('AuthService', () => {
  let _service: AuthenticationService; 
  
  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [HttpClientModule, HttpClientTestingModule],
      providers: [AuthenticationService]
    });

    _service = TestBed.get(AuthenticationService);

  });

  it('should be created', () => {
    expect(_service).toBeTruthy();
  });

  it('should register user data', fakeAsync(() => {
    let data = raw.registerUser;
    inject([AuthenticationService, HttpTestingController], (Backend: HttpTestingController) => {
      const mockReq = Backend.expectOne(_service.springEndPoint);
      expect(mockReq.request.url).toEqual(_service.springEndPoint, 'url must be equal to request url in service');
      expect(mockReq.request.method).toBe('POST', 'method type should be post for register');
      mockReq.flush(data);
      Backend.verify();
    });

    _service.registerUser(data).subscribe((res: any)=> {
      expect(res).toBeDefined();
      expect(res._body).toBe(data, 'response data should be same');
    });

  }));

  it('should login user', fakeAsync(() => {
    let data = raw.loginUser;

    inject([AuthenticationService, HttpTestingController], (Backend: HttpTestingController) => {
      const mockReq = Backend.expectOne(_service.springEndPoint);
      expect(mockReq.request.url).toEqual(_service.springEndPoint, 'url must be equal to request url in service');
      expect(mockReq.request.method).toBe('POST', 'method type should be post for login');
      mockReq.flush(data);
      Backend.verify();
    });

    _service.loginUser(data).subscribe((res: any)=> {
      expect(res).toBeDefined();
      expect(res._body).toBe(data, 'response data should be same');
    });

  }));
});