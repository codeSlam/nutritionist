import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterComponent } from './register.component';
import { AngularMaterialModule } from 'src/app/modules/shared/angular-material/angular-material.module';
import { RouterTestingModule } from '@angular/router/testing';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../auth.service';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';

const raw = {
  firstName: "john",
  lastName: "tanny",
  userId: "tanny1",
  password: "1234"
}

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let _service: AuthenticationService;
  let fixture: ComponentFixture<RegisterComponent>;
  let spyUser: any;
  let routes: Router;
  let location: Location;

  class AuthenticationServiceStub {
    currentUser: any;
    
    constructor() {}

    registerUser(data) {

      if(data == raw) {
        return of(data);
      }

      return of(false);

    }
  }

  class dummy {

  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterComponent ],
      imports: [AngularMaterialModule,
        RouterTestingModule.withRoutes([{path: '', component: dummy}])
      ],
      providers: [{provide: AuthenticationService, useClass: AuthenticationServiceStub}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    fixture.debugElement.injector.get(AuthenticationService);
    location = TestBed.get(Location);
    routes = TestBed.get(Router);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should all the fields of registeration form', () => {
    let firstName = fixture.debugElement.query(By.css('.user-fname'));
    let lastName = fixture.debugElement.query(By.css('.user-lname'));
    let userId = fixture.debugElement.query(By.css('.user-id'));
    let userPassword = fixture.debugElement.query(By.css('.user-password'));
    let regBtn = fixture.debugElement.query(By.css('.register-user'));
    let registerBtn = fixture.debugElement.query(By.css('.login-btn'));
    
    expect(firstName.nativeElement).toBeTruthy();
    expect(lastName.nativeElement).toBeTruthy();
    expect(userId.nativeElement).toBeTruthy();
    expect(userPassword.nativeElement).toBeTruthy();
    expect(regBtn.nativeElement).toBeTruthy();
    expect(registerBtn.nativeElement).toBeTruthy();
  })

  it('should Register user successfully', async(() => {

    let firstName = fixture.debugElement
    .query(By.css('.user-fname')).nativeElement;

    let lastName = fixture.debugElement
    .query(By.css('.user-lname')).nativeElement;

    let userId = fixture.debugElement
    .query(By.css('.user-id')).nativeElement;
    
    let userPassword = fixture.debugElement
    .query(By.css('.user-password')).nativeElement;

    let regBtn = fixture.debugElement
    .query(By.css('.register-user')).nativeElement;

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      firstName.value = 'john';
      lastName.value = "tanny"
      userId.value = 'tanny1';
      userPassword.value = '1234'
      firstName.dispatchEvent(new Event('input'))
      lastName.dispatchEvent(new Event('input'))
      userId.dispatchEvent(new Event('input'));
      userPassword.dispatchEvent(new Event('input'));
      regBtn.click();

    }).then(() => {
      expect(location.path()).toBe('');
    });

  }));

});
