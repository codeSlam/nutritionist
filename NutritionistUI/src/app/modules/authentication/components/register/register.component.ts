import { Component, OnInit } from '@angular/core';
import { User } from '../../user.model';
import { AuthenticationService } from '../../auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'auth-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: User;
  disabled: boolean;

  constructor(private _service: AuthenticationService, 
    private _snackBar: MatSnackBar,
    private router: Router) {

    this.user = new User();
    this.disabled = false;

   }

  ngOnInit() {
  }

  registerUser() {

    this.disabled = true;

    this._service.registerUser(this.user)
    .subscribe((response: any) => {
      let matSnackBarRef = this._snackBar.open(response.message, 'Login',
        {
          duration: 4000
        });

        matSnackBarRef.onAction().subscribe(res => {
          matSnackBarRef.dismiss();
        });

        matSnackBarRef.afterDismissed().subscribe(res => {
          this.router.navigate(['/login']);
        });

    },
    error => {
      this.disabled = false;
      let matSnackBarRef = this._snackBar.open(error.error.message, '',
        {
          duration: 2500
        });
    });

  }

}