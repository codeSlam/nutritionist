import { Component, OnInit } from '@angular/core';
import { User } from '../../user.model';
import { AuthenticationService } from '../../auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'auth-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User;

  constructor(private _service: AuthenticationService, 
    private _snackBar: MatSnackBar,
    private router: Router) {
       
    this.user = new User();
  }

  ngOnInit() {
  }

  login() {
    this._service.loginUser(this.user)
    .subscribe((response: any) => {
        
        this._service.emitLogin();
        this._service.saveToken(response.token);
        this.router.navigate(['/food']);

    },
    error => {

      if(error.status == 500) {

        let snackBarRef = this._snackBar.open(error.error.message, 'Register',
        {
          duration: 4000
        });

        snackBarRef.onAction().subscribe( res => {
          this.router.navigate(['/register']);
        });

      } else {

        this._snackBar.open(error.error.message, '',
        {
          duration: 2500
        });

      }
    
    });
  }

}
