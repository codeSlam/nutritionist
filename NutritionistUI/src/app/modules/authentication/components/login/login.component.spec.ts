import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { AngularMaterialModule } from 'src/app/modules/shared/angular-material/angular-material.module';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthenticationService } from '../../auth.service';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { Location } from '@angular/common';

const raw = {
  userData: {
    firstName: "john",
    lastName: "tanny",
    userId: "testuser",
    password: "qwerty"
  }
}

describe('LoginComponent', () => {
  let component: LoginComponent;
  let _service: AuthenticationService;
  let fixture: ComponentFixture<LoginComponent>;
  let spyUser: any;
  let routes: Router;
  let location: Location;

  class AuthenticationServiceStub {
    currentUser: any;
    
    constructor() {}

    login(data) {

      if(data.userId == raw.userData.userId) {
        
        return of(data.userId);

      }

      return of(false);

    }
  }

  class dummy {

  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [AngularMaterialModule,
        RouterTestingModule.withRoutes([{path: '', component: dummy}])
      ],
      providers: [{provide: AuthenticationService, useClass: AuthenticationServiceStub}]
    })
    .compileComponents();
  }));

  beforeEach(() => {

    fixture = TestBed.createComponent(LoginComponent);
    fixture.debugElement.injector.get(AuthenticationService);
    location = TestBed.get(Location);
    routes = TestBed.get(Router);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain two input box for user ID and password', () => {
    let userId = fixture.debugElement.query(By.css('.user-id'));
    let userPassword = fixture.debugElement.query(By.css('.user-password'));
    let loginBtn = fixture.debugElement.query(By.css('.login-user'));
    let registerBtn = fixture.debugElement.query(By.css('.register-btn'));

    expect(userId.nativeElement).toBeTruthy();
    expect(userPassword.nativeElement).toBeTruthy();
    expect(loginBtn.nativeElement).toBeTruthy();
    expect(registerBtn.nativeElement).toBeTruthy();
  })

  it('should login in successfully', async(() => {
    let userId = fixture.debugElement
    .query(By.css('.user-id')).nativeElement;
    
    let userPassword = fixture.debugElement
    .query(By.css('.user-password')).nativeElement;

    let loginBtn = fixture.debugElement
    .query(By.css('.login-user')).nativeElement;

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      userId.value = 'testuser';
      userPassword.value = 'qwerty'
      userId.dispatchEvent(new Event('input'));
      userPassword.dispatchEvent(new Event('input'));
      loginBtn.click();

    }).then(() => {
      expect(location.path()).toBe('');
    });

  }));
  
});