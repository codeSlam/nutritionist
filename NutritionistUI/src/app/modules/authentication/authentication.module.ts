import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthenticationRoutingModule } from './authentication-routing.module';
import { AngularMaterialModule } from '../shared/angular-material/angular-material.module';
import { AuthenticationService } from './auth.service';

@NgModule({
  declarations: [
    LoginComponent, 
    RegisterComponent
  ],
  imports: [
    CommonModule,
    AuthenticationRoutingModule,
    AngularMaterialModule
  ],
  providers: [
    AuthenticationService
  ]
})
export class AuthenticationModule { }