import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { User } from './user.model';
import * as jwt_decode from 'jwt-decode';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: "root"
})
export class AuthenticationService {

    springEndPoint: string;

    userLoggedIn = new Subject<any>();
    loginChangeObservable = this.userLoggedIn.asObservable();

    constructor(private _http: HttpClient) {
        this.springEndPoint = "http://localhost:9091/api/v1/auth/";
    }

    registerUser(user: User) {
        return this._http.post(`${this.springEndPoint}register`, user);
    }

    loginUser(user: User) {
        return this._http.post(`${this.springEndPoint}login`, user);
    }

    saveToken(token) {
        return localStorage.setItem("jwt", token);
    }

    getToken() {
        return localStorage.getItem("jwt");
    }

    removeToken() {
        return localStorage.removeItem("jwt");
    }

    getTokenExpirationDate(token: string) {
        const decoded = jwt_decode(token);
        if(decoded.exp === undefined) return null;
        const date = new Date(0);
        date.setUTCSeconds(decoded.exp);
        return date;
      }
    
      isTokenExpired(token?: string): boolean {
        if(!token) token = this.getToken();
        if(!token) return true;
        const date = this.getTokenExpirationDate(token);
        if(date === undefined || date ===null) return false;
        return !(date.valueOf() > new Date().valueOf());
      }

      getSubject() {
        const decoded = jwt_decode(localStorage.getItem('jwt'));
        return decoded.sub;
      }
    
      emitLogin() {
          this.userLoggedIn.next();
      }
}