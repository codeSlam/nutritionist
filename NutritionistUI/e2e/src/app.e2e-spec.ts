import { AppPage } from './app.po';
import { browser, logging, by, protractor, element } from 'protractor';

describe('Nutritionist', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display Titile', () => {
    page.navigateTo();
    expect(browser.getTitle()).toEqual('NutritionistUI');
  });

  it('should redirect to login page on opening the application', () => {
    expect(browser.getCurrentUrl()).toContain('/login');
  });

  it('should be rediredted to register page', () => {
    browser.element(by.css('.register-btn')).click();
    expect(browser.getCurrentUrl()).toContain('/register');
  });

  it('should register user', () => {
    browser.element(by.css(".user-fname")).sendKeys("john");
    browser.element(by.css(".user-lname")).sendKeys("tanny");
    browser.element(by.css(".user-id")).sendKeys("tanny1");
    browser.element(by.css(".user-password")).sendKeys("qwerty");
    browser.element(by.css('.register-user')).click();
    browser.driver.sleep(5000);
    expect(browser.getCurrentUrl()).toContain('/login');
  });

  it('should be able to login and navigate to Favourites food', () => {
    browser.element(by.css(".user-id")).sendKeys("tanny1");
    browser.element(by.css(".user-password")).sendKeys("qwerty");
    browser.element(by.css(".login-user")).click();
    expect(browser.getCurrentUrl()).toContain('/food/favourites');
  });

  it('should navigate to search page', () => {
    browser.element(by.css(".search-btn")).click();
    expect(browser.getCurrentUrl()).toContain('/food/search');
  });

  it('should perform search', () => {
    browser.element(by.css('.search-box')).sendKeys("ghee");
    browser.element(by.css('.search-box')).sendKeys(protractor.Key.ENTER);
    const searchItems = browser.element.all(by.css(".food-title"));
    expect(searchItems.get(0).getText()).toContain("GHEE");
  });

  it('should save to database', () => {
    browser.element(by.css(".add-fav")).click();
    browser.get('/food/favourites');
    const watchlistItems = browser.element(by.css(".food-title"));
    expect(watchlistItems.getText()).toContain("GHEE");
  });

  it('should update name of item in database', async () => {
    browser.element(by.css('.update-btn')).click();
    browser.element(by.css('#food-name')).sendKeys("milk");
    browser.element(by.css('.update-name')).click();
    const name = browser.element(by.css(".food-title"));
    expect(name.getText()).toContain("milk");
  });

  it('should delete movie in database', async () => {
    browser.driver.sleep(3000);
    browser.element(by.css('.delete-item')).click();
    var myElement = element(by.css('#food-name'));
    expect(myElement.isPresent()).toBeFalsy();
  });

  it('should logout', () => {
    browser.element(by.css('.logout-btn')).click();
    expect(browser.getCurrentUrl()).toContain('/login');
  });

  it('should not be able to access other links without login', () => {
    browser.get('/food/favourites');
    expect(browser.getCurrentUrl()).toContain('/login');
    browser.get('/food/search');
    expect(browser.getCurrentUrl()).toContain('/login');
  })

});