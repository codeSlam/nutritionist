export MYSQL_DATABASE=fooddb
export MYSQL_USER=root
export MYSQL_PASSWORD=root
export DDL_CONFIG=create-drop
if [[ -z "${MYSQL_CI_URL}" ]]; then
	export MYSQL_CI_URL=jdbc:mysql://localhost:3306/fooddb?useSSL=false
fi
